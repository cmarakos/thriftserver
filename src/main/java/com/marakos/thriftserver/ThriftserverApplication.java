package com.marakos.thriftserver;

import com.marakos.thriftserver.com.thrift.impl.ThriftServerImp;
import com.marakos.thriftserver.com.thrift.impl.ThriftService;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class ThriftserverApplication {
    public static void main(String[] args) {
        SpringApplication.run(ThriftserverApplication.class, args);
    }
}
