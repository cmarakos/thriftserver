package com.marakos.thriftserver.com.thrift.impl;

import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class ThriftServer {

    ThriftServerImp handler;
    ThriftService.Processor processor;

    public ThriftServer(ThriftServerImp handler) {
        this.handler = handler;
        this.processor =  new ThriftService.Processor(handler);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void initServer(){

        Runnable simple = new Runnable() {
            @Override
            public void run() {
                simple(processor);
            }
        };

        new Thread(simple).start();

    }

    public void simple(ThriftService.Processor processor) {
        try {
            TServerTransport serverTransport = new TServerSocket(9090);
            TServer server = new TSimpleServer(new TServer.Args(serverTransport).processor(processor));

            // Use this for a multithreaded server
            // TServer server = new TThreadPoolServer(new TThreadPoolServer.Args(serverTransport).processor(processor));

            System.out.println("Starting the simple server...");
            server.serve();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
