package com.marakos.thriftserver.com.thrift.impl;

import com.marakos.thriftserver.Controller.KafkaProducerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ThriftServerImp implements ThriftService.Iface {

    @Autowired
    KafkaProducerController kafkaProducerController;

    public ThriftServerImp(KafkaProducerController kafkaProducerController) {
        this.kafkaProducerController = kafkaProducerController;
    }

    @Override
    public void sendLogEvent(Message message) {
        System.out.println("Hey i got a new message: "+message.getLogEvent().getMessage());
        kafkaProducerController.sendMessage(message);
    }
}
