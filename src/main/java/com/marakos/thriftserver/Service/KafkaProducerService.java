package com.marakos.thriftserver.Service;

import com.marakos.thriftserver.com.thrift.impl.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducerService {
    @Autowired
    private KafkaTemplate<String, Message> kafkaTemplate;

    @Value("${kafka.topic}")
    private  String kafkaTopic;

    public void send(Message message) {
        kafkaTemplate.send(kafkaTopic, message);
    }
}
