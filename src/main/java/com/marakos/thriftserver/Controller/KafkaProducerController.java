package com.marakos.thriftserver.Controller;

import com.marakos.thriftserver.Service.KafkaProducerService;
import com.marakos.thriftserver.com.thrift.impl.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class KafkaProducerController {
    @Autowired
    KafkaProducerService kafkaProducer;

    Logger logger = LogManager.getLogger(KafkaProducerController.class);

    public KafkaProducerController(KafkaProducerService kafkaProducer) {
        this.kafkaProducer = kafkaProducer;
    }

    public void sendMessage(Message message)
    {
        logger.info("Received message with id: "+ message.getId());
        try{
            kafkaProducer.send(message);
            logger.info("Message sent Successfully to the Kafka topic EventLogs-topic");
        }catch (Exception e){
            logger.error("There was an error sending to Kafka the message with id: "+message.getId());
        }

    }


}
